//
//  ViewController.swift
//  ExpenseTracker
//
//  Created by Garrick McMickell on 10/12/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class ViewController: UITabBarController {
    
    var arrayOfExpenses = [[String:AnyObject]]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = UserDefaults.standard
        
        if (defaults.array(forKey: "expenses") != nil) {
            arrayOfExpenses = defaults.array(forKey: "expenses") as! [[String : AnyObject]]
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.dismissAddExpenseVC), name: NSNotification.Name(rawValue: "dismissAddExpenseVC"), object: nil)
    }
    
    func dismissAddExpenseVC(notif: Notification) -> Void {
        if (notif.object != nil) {
            arrayOfExpenses.append(notif.object as! [String:AnyObject])
            
            let defaults = UserDefaults.standard
            defaults.set(arrayOfExpenses, forKey: "expenses")
        }
        
        self.dismiss(animated: true) {
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

