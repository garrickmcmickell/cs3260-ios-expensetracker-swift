//
//  Tab1ViewController.swift
//  ExpenseTracker
//
//  Created by Garrick McMickell on 10/14/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class Tab1ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var vc = ViewController()
    var arrayOfExpenses = [[String:AnyObject]]()    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        vc = self.tabBarController as! ViewController
        arrayOfExpenses = vc.arrayOfExpenses
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        arrayOfExpenses = vc.arrayOfExpenses
        
        arrayOfExpenses.sort{
            (($0 )["actualDate"] as? Date)! < (($1 )["actualDate"] as? Date)!
        }
        
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfExpenses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let dictionary = arrayOfExpenses[indexPath.row]
        
        cell.textLabel?.text = (dictionary["date"] as! String?)! + "      $" + (dictionary["amount"] as! String?)!
        
        return cell
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
