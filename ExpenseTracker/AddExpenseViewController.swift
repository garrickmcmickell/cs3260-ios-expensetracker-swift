//
//  AddExpenseViewController.swift
//  ExpenseTracker
//
//  Created by Garrick McMickell on 10/12/16.
//  Copyright © 2016 Garrick McMickell. All rights reserved.
//

import UIKit

class AddExpenseViewController: UIViewController {

    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var amountTextField: UITextField!
    @IBOutlet var locationTextField: UITextField!
    @IBOutlet var categoryTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(AddExpenseViewController.adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AddExpenseViewController.adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func SaveBtnTouched(_ sender: AnyObject) {
        if (amountTextField.text == "" || categoryTextField.text == "") {
            let alert = UIAlertController(title: "Alert", message: "Please fill in required fields.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            if (amountTextField.text == "") {
                amountTextField.layer.borderWidth = 1
                amountTextField.layer.cornerRadius = 5
                amountTextField.layer.borderColor = UIColor.red.cgColor
            }
            else {
                amountTextField.layer.borderWidth = 0
            }
            
            if (categoryTextField.text == "") {
                categoryTextField.layer.borderWidth = 1
                categoryTextField.layer.cornerRadius = 5
                categoryTextField.layer.borderColor = UIColor.red.cgColor
            }
            else {
                categoryTextField.layer.borderWidth = 0
            }
        }
        else {
            let dateFormat = DateFormatter()
            dateFormat.dateStyle = DateFormatter.Style.short
            dateFormat.timeStyle = DateFormatter.Style.none
            let date = dateFormat.string(from: datePicker.date)
        
            let dictionary: [String:AnyObject] = ["date":date as AnyObject,
                                                  "actualDate":datePicker.date as AnyObject!,
                                                  "amount":amountTextField.text as AnyObject!,
                                                  "location":locationTextField.text as AnyObject!,
                                                  "category":categoryTextField.text as AnyObject!]
        
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissAddExpenseVC"), object: dictionary)
        }
    }
    
    @IBAction func CancelBtnTouched(_ sender: AnyObject) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dismissAddExpenseVC"), object: nil)
    }
    
    func adjustForKeyboard(notif: Notification) -> Void {
        let userInfo = notif.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notif.name == Notification.Name.UIKeyboardWillHide {
            scrollView.contentInset = UIEdgeInsets.zero
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        
        scrollView.scrollIndicatorInsets = scrollView.contentInset        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
